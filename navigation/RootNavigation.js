import { Notifications } from 'expo';
import React from 'react';
import { StackNavigator } from 'react-navigation';

import FirstScreen from '../screens/FirstScreen';
import AuthScreen from '../screens/AuthScreen';
import SecondScreen from '../screens/SecondScreen';
import HeroProfileScreen from '../screens/HeroProfileScreen';
import HeroListScreen from '../screens/HeroListScreen';
import CreateProjectScreen from '../screens/CreateProjectScreen';
import ProjectScreen from '../screens/ProjectScreen';
import registerForPushNotificationsAsync from '../api/registerForPushNotificationsAsync';

const RootStackNavigator = StackNavigator(
  {
    First: {
      screen: FirstScreen,
    },
    Auth: {
      screen: AuthScreen,
    },
    Second: {
      screen: SecondScreen,
    },
    HeroProfile: {
      screen: HeroProfileScreen,
    },
    HeroList: {
      screen: HeroListScreen,
    },
    CreateProject: {
      screen: CreateProjectScreen,
    },
    Project: {
      screen: ProjectScreen,
    },
  },
  {
    navigationOptions: () => ({
      headerTitleStyle: {
        fontWeight: 'normal',
      },
    }),
  }
);

export default class RootNavigator extends React.Component {
  componentDidMount() {
    this._notificationSubscription = this._registerForPushNotifications();
  }

  componentWillUnmount() {
    this._notificationSubscription && this._notificationSubscription.remove();
  }

  render() {
    return <RootStackNavigator />;
  }

  _registerForPushNotifications() {
    // Send our push token over to our backend so we can receive notifications
    // You can comment the following line out if you want to stop receiving
    // a notification every time you open the app. Check out the source
    // for this function in api/registerForPushNotificationsAsync.js
    registerForPushNotificationsAsync();

    // Watch for incoming notifications
    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    );
  }

  _handleNotification = ({ origin, data }) => {
    console.log(
      `Push notification ${origin} with data: ${JSON.stringify(data)}`
    );
  };
}
