import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  AsyncStorage
} from 'react-native';
import { WebBrowser } from 'expo';

export default class HeroProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: '' };
  }

  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <Image source={require('../assets/images/third_bg.jpg')} style={styles.backgroundImage}>
        <Text style={styles.captionText}>Профиль</Text>
        <Text style={styles.inputNameCaption}>Имя:</Text>
        <TextInput style={styles.inputName}
          onChangeText={(name) => this.setState({name})}
          value={this.state.name}>
        </TextInput>
        <View style={styles.nextContainer}>
          <TouchableOpacity
            onPress={this._onPressNext}
            style={styles.nextLink}>
            <Text style={styles.nextLinkText}>Стать героем</Text>
          </TouchableOpacity>
        </View>
      </Image>
    );
  }

  _onPressNext = async () => {
    const { navigate } = this.props.navigation;
    let token = await AsyncStorage.getItem('token');
    let account = await AsyncStorage.getItem('account');
    account = JSON.parse(account);

    let res = await fetch('http://176.112.198.197/api/db/accounts/' + account.id, {
      method: 'PUT',
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.name
      })
    }).catch(e => console.error(e));
    res = await res.json();

    if (!res.data) {
      console.error(res.error);
      return navigate('Auth');
    }

    await AsyncStorage.setItem('account', JSON.stringify(res.data));
    navigate('HeroList');
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    width: null,
    height: null,
  },
  captionText: {
    fontFamily: "minsk",
    fontSize: 40,
    marginTop: 40,
    paddingTop: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#006073'
  },
  inputNameCaption: {
    fontFamily: "akrobat",
    fontSize: 20,
    marginTop: 30,
    marginHorizontal: 20,
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#006073'
  },
  inputName: {
    marginHorizontal: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    borderColor: '#fbd78f',
    borderWidth: 1,
    backgroundColor: '#ffffff',
  },
  nextContainer: {
    marginTop: 300,
    alignItems: 'center',
  },
  nextLink: {
    backgroundColor: '#fbd78f',
    height: 40,
    width: 290,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  nextLinkText: {
    fontFamily: "akrobat",
    fontSize: 19,
    color: '#006073',
  },
});
