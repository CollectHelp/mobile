import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  AsyncStorage
} from 'react-native';
import { WebBrowser } from 'expo';

export default class HeroListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { projects: [] };
  }

  static navigationOptions = {
    header: null,
  };

  async componentWillMount() {
    let token = await AsyncStorage.getItem('token');
    let res = await fetch('http://176.112.198.197/api/db/projects/', {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    }).catch(e => console.error(e));
    res = await res.json();

    this.setState({projects: res.data});
  }

  render() {

    return (
      <Image source={require('../assets/images/second_bg.jpg')} style={styles.backgroundImage}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}>
          <Text style={styles.captionText}>Проекты</Text>
          <View style={styles.projects}>
            <View style={styles.col1}>
              {this.state.projects.filter((p,i) => !(i%2)).map(project => this._renderProject(project))}
            </View>
            <View style={styles.col2}>
              {this.state.projects.filter((p,i) => (i%2)).map(project => this._renderProject(project))}
            </View>
          </View>
        </ScrollView>
      </Image>
    );
  }

  _renderProject(project) {
    return (
      <TouchableOpacity key={project.id} style={styles.project} onPress={this._onPressProject.bind(this, project)}>
        <Image source={{uri: (project.image || '../assets/images/robot-prod.png')}} style={styles.projectImage}>
          <Text style={styles.projectName}>{project.name}</Text>
        </Image>
      </TouchableOpacity>
    );
  }

  _onPressProject = (project) => {
    const { navigate } = this.props.navigation;
    navigate('Project', { project });
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    width: null,
    height: null,
  },
  contentContainer: {
    marginTop: 30,
  },
  captionText: {
    fontFamily: "minsk",
    fontSize: 30,
    paddingTop: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#006073'
  },
  projects: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  col1: {
    flex: 1,
    width: 150,
    marginHorizontal: 5,
    marginTop: 20,
    justifyContent: 'flex-start',
  },
  col2: {
    flex: 1,
    width: 150,
    marginTop: 70,
    marginHorizontal: 5,
    justifyContent: 'flex-start',
  },
  project: {
    width: 140,
    height: 140,
    margin: 5,
  },
  projectImage: {
    width: 140,
    height: 140,
    resizeMode: 'contain',
    padding: 10,
  },
  projectName: {
    fontSize: 13,
    width: 80,
    padding: 5,
    borderRadius: 5,
    backgroundColor: 'rgba(255,255,255,0.7)',
  },
});
