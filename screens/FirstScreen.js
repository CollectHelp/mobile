import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';

export default class FirstScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <Image source={require('../assets/images/first_bg.jpg')} style={styles.backgroundImage}>
        <Text style={styles.captionText1}>Collect</Text>
        <Text style={styles.captionText2}>Help</Text>
        <Text style={styles.captionText3}>Помогаем друг другу</Text>
        <Text style={styles.captionText4}>воплотить мечту</Text>
        <View style={styles.nextContainer}>
          <TouchableOpacity
            onPress={this._onPressNext}
            style={styles.nextLink}>
            <Text style={styles.nextLinkText}>Перейти к мечтам</Text>
          </TouchableOpacity>
        </View>
      </Image>
    );
  }

  _onPressNext = () => {
    this.props.navigation.navigate('Auth');
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    width: null,
    height: null,
  },
  captionText1: {
    fontFamily: "minsk",
    fontSize: 50,
    marginTop: 50,
    paddingTop: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#006073'
  },
  captionText2: {
    fontFamily: "minsk",
    fontSize: 50,
    paddingTop: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#006073'
  },
  captionText3: {
    fontFamily: "minsk",
    fontSize: 15.5,
    paddingTop: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#006073'
  },
  captionText4: {
    fontFamily: "minsk",
    fontSize: 15.5,
    paddingTop: 10,
    paddingLeft: 60,
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#006073'
  },
  nextContainer: {
    marginTop: 300,
    alignItems: 'center',
  },
  nextLink: {
    backgroundColor: '#fbd78f',
    height: 40,
    width: 290,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  nextLinkText: {
    fontFamily: "akrobat",
    fontSize: 19,
    color: '#006073',
  },
});
