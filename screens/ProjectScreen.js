import React from 'react';
import {
  Image,
  StyleSheet,
  AsyncStorage,
  Text,
  TouchableOpacity,
  View,
  LinkingIOS
} from 'react-native';
import { WebBrowser } from 'expo';

export default class AuthScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { project: {} };
  }

  static navigationOptions = {
    header: null,
  };

  async componentWillMount() {
    let { project } = this.props.navigation.state.params;
    let token = await AsyncStorage.getItem('token');
    let res = await fetch('http://176.112.198.197/api/db/projects/' + project.id, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    }).catch(e => console.error(e));
    res = await res.json();

    this.setState({project: res.data});
  }

  render() {
    return (
      <Image source={require('../assets/images/second_bg.jpg')} style={styles.backgroundImage}>
        <View style={styles.project} >
          <Image source={{uri: (this.state.project.image || '../assets/images/robot-prod.png')}} style={styles.projectImage}>
            <Text style={styles.projectName}>{this.state.project.name}</Text>
          </Image>
        </View>
        <View style={styles.text}>
          <Text style={styles.textIn}>Необходимые ресурсы</Text>
        </View>
        <View style={styles.row1}>
          {(this.state.project.resources || []).slice(0, 3).map(resource => this._renderResoure(resource))}
        </View>
        <View style={styles.row2}>
          {(this.state.project.resources || []).slice(3, 6).map(resource => this._renderResource(resource))}
        </View>
        <View style={styles.row3}>
          {(this.state.project.resources || []).slice(6).map(resource => this._renderResource(resource))}
        </View>
        <View style={styles.nextContainer}>
          <TouchableOpacity
            onPress={this._onPressNext}
            style={styles.nextLink}>
            <Text style={styles.nextLinkText}>Принять участие</Text>
          </TouchableOpacity>
        </View>
      </Image>
    );
  }

  _onPressNext = async () => {
  }

  _renderResoure(resource) {
    return (<View key={resource.id}></View>);
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    width: null,
    height: null,
  },
  imageContainer: {
    alignItems: 'center',
    marginTop: 200,
  },
  project: {
    width: 400,
    height: 200
  },
  projectImage: {
    width: 400,
    height: 200,
    resizeMode: 'cover',
  },
  projectName: {
    fontSize: 13,
    width: 150,
    padding: 15,
    margin: 20,
    borderRadius: 10,
    backgroundColor: 'rgba(255,255,255,0.7)',
  },
  text: {
    marginTop: 20,
    alignItems: 'center',
  },
  textIn: {
    fontFamily: "akrobat",
    color: '#006073',
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  nextContainer: {
    marginTop: 175,
    alignItems: 'center',
  },
  nextLink: {
    backgroundColor: '#fbd78f',
    height: 40,
    width: 290,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  nextLinkText: {
    fontFamily: "akrobat",
    fontSize: 19,
    color: '#006073',
    justifyContent: 'center',
  },
});
