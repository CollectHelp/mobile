import React from 'react';
import {
  Image,
  Button,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  AsyncStorage
} from 'react-native';
import { WebBrowser } from 'expo';

import Swiper from 'react-native-swiper';

export default class SecondScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <Image source={require('../assets/images/second_bg.jpg')} style={styles.backgroundImage}>
        <Swiper showsButtons={false} loop={false} dotColor={'#006074'} activeDotColor={'rgba(0,96,116,.5)'}>
          <View>
            <View style={styles.caption}>
              <Text style={styles.captionText}>От идеи</Text>
              <Text style={styles.captionText}>до воплощения</Text>
              <Text style={styles.captionText}>один шаг!</Text>
            </View>
            <View style={styles.imageContainer}>
              <Image source={require('../assets/images/children.png')} style={styles.centerImage}></Image>
            </View>
            <View style={styles.text}>
              <Text style={styles.textIn}>Хочешь воплотить свою мечту, но чего-то не хватает?</Text>
              <Text style={styles.textIn}> </Text>
              <Text style={styles.textIn}>На помощь придут</Text>
              <Text style={styles.textIn}>единомышленники, готовые поддержать твою</Text>
              <Text style={styles.textIn}>идею</Text>
            </View>
          </View>
          <View>
            <View style={styles.caption}>
              <Text style={styles.captionText}>Стань</Text>
              <Text style={styles.captionText}>героем</Text>
              <Text style={styles.captionText}>для других!</Text>
            </View>
            <View style={styles.imageContainer}>
              <Image source={require('../assets/images/superman.png')} style={styles.centerImage}></Image>
            </View>
            <View style={styles.text}>
              <Text style={styles.textIn}>Вноси свой вклад</Text>
              <Text style={styles.textIn}>в интересные проекты</Text>
            </View>
          </View>
          <View>
            <View style={styles.caption}>
              <Text style={styles.captionText}> </Text>
              <Text style={styles.captionText}> </Text>
              <Text style={styles.captionText}> </Text>
            </View>
            <View style={styles.imageContainer}>
              <Image source={require('../assets/images/gagarin.png')} style={styles.centerImage}></Image>
            </View>
            <View style={styles.text}>
              <Text style={styles.textIn}>Поехали!</Text>
            </View>
            <View style={styles.buttons}>
              <TouchableOpacity
                onPress={this._onPressDream}
                style={styles.button}>
                <Text style={styles.buttonText}>Реализовать идею</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this._onPressHero}
                style={styles.button}>
                <Text style={styles.buttonText}>Стать героем</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Swiper>
      </Image>
    );
  }

  _onPressDream = () => {
    this.props.navigation.navigate('CreateProject');
  }
  _onPressHero = async () => {
    try {
      let account = await AsyncStorage.getItem('account').catch(e => console.error(e));
      console.log(account);
      account = account && JSON.parse(account);

      if (account.name) {
        this.props.navigation.navigate('HeroList');
      } else {
        this.props.navigation.navigate('HeroProfile');
      }
    } catch(e) {
      console.error(e);
      console.log(e);
    }
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    width: null,
    height: null,
    alignItems: 'center',
  },
  caption: {
    alignItems: 'flex-end',
    marginTop: 30,
    marginRight: 20,
  },
  captionText: {
    fontFamily: "minsk",
    fontSize: 30,
    paddingTop: 5,
    color: '#fff'
  },
  imageContainer: {
    alignItems: 'center',
    marginTop: 90,
  },
  centerImage: {
    width: 100,
    height: 110,
    resizeMode: 'contain',
  },
  text: {
    marginTop: 20,
    alignItems: 'center',
  },
  textIn: {
    fontFamily: "akrobat",
    color: '#006073',
    textAlign: 'center',
  },
  buttons: {
    flex: 1,
    marginTop: 100,
    alignItems: 'center',
    flexDirection: 'row',
  },
  button: {
    width: null,
    height: 40,
    marginHorizontal: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    backgroundColor: '#fbd78f',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: 'akrobat',
    fontSize: 19,
    color: '#006073',
  },
});
