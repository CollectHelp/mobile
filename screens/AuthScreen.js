import React from 'react';
import {
  Image,
  StyleSheet,
  AsyncStorage,
  Text,
  TouchableOpacity,
  View,
  LinkingIOS
} from 'react-native';
import { WebBrowser } from 'expo';

export default class AuthScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <Image source={require('../assets/images/second_bg.jpg')} style={styles.backgroundImage}>
        <View style={styles.imageContainer}>
          <Image source={require('../assets/images/people_auth.png')} style={styles.centerImage}></Image>
        </View>
        <View style={styles.text}>
          <Text style={styles.textIn}>Авторизуйтесь!</Text>
        </View>
        <View style={styles.nextContainer}>
          <TouchableOpacity
            onPress={this._onPressNext}
            style={styles.nextLink}>
            <Text style={styles.nextLinkText}><Image source={require('../assets/images/vk.png')} style={styles.vkImage}></Image></Text>
          </TouchableOpacity>
        </View>
      </Image>
    );
  }

  _onPressNext = async () => {
    let { navigate } = this.props.navigation;
    let { setState } = this;

    let res = await fetch('http://176.112.198.197/api/auth/login/social', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        token: 'd5dc5c20a8ba01bf16954529ad738d1a1c7ff00fc7fe1837395c346571713db3ff2855b0b0d283954712d'
      })
    }).catch(e => console.error(e));
    res = await res.json();

    await AsyncStorage.setItem('token', res.access_token);
    await AsyncStorage.setItem('account', JSON.stringify(res.account.data));

    navigate('Second');
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    width: null,
    height: null,
  },
  imageContainer: {
    alignItems: 'center',
    marginTop: 200,
  },
  centerImage: {
    width: 100,
    height: 110,
    resizeMode: 'contain',
  },
  text: {
    marginTop: 20,
    alignItems: 'center',
  },
  textIn: {
    fontFamily: "akrobat",
    color: '#006073',
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  nextContainer: {
    marginTop: 175,
    alignItems: 'center',
  },
  nextLink: {
    backgroundColor: '#fbd78f',
    height: 40,
    width: 290,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  nextLinkText: {
    fontFamily: "akrobat",
    fontSize: 19,
    color: '#006073',
    justifyContent: 'center',
  },
  vkImage: {
    height: 30,
    marginTop: 3,
    resizeMode: 'contain',
  },
});
