import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  AsyncStorage,
  ScrollView
} from 'react-native';
import { WebBrowser } from 'expo';

export default class HeroProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: '', description: '', resources: '' };
  }

  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <Image source={require('../assets/images/third_bg.jpg')} style={styles.backgroundImage}>
        <ScrollView>
          <Text style={styles.captionText}>Мой проект</Text>
          <Text style={styles.inputNameCaption}>Цель проекта:</Text>
          <TextInput style={styles.inputName}
            onChangeText={(name) => this.setState({name})}
            value={this.state.name} multiline={true} />
          <Text style={styles.inputNameCaption}>Задачи проекта:</Text>
          <TextInput style={styles.inputName}
            onChangeText={(description) => this.setState({description})}
            value={this.state.description} multiline={true} />
          <Text style={styles.inputNameCaption}>Необходимые ресурсы:</Text>
          <TextInput style={styles.inputName}
            onChangeText={(resources) => this.setState({resources})}
            value={this.state.resources} multiline={true} />
          <View style={styles.nextContainer}>
            <TouchableOpacity
              onPress={this._onPressNext}
              style={styles.nextLink}>
              <Text style={styles.nextLinkText}>Зарегестрировать проект</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Image>
    );
  }

  _onPressNext = async () => {
    const { navigate } = this.props.navigation;
    let token = await AsyncStorage.getItem('token');
    let account = await AsyncStorage.getItem('account');
    account = JSON.parse(account);

    let types = [
      'people', 'media', 'brain',
      'auto', 'cook', 'repair',
      'money', 'education', 'clothes', 'home', 'food'
    ];

    let res = await fetch('http://176.112.198.197/api/db/projects/', {
      method: 'POST',
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.name,
        description: this.state.description,
        image: '',
        resources: this.state.resources.split(',').map(res => ({
          name: res,
          type: types.shift(),
          status: 'open'
        }))
      })
    }).catch(e => console.error(e));
    res = await res.json();

    if (!res.data) {
      console.error(res.error);
      return navigate('Auth');
    }

    navigate('Second');
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    width: null,
    height: null,
  },
  captionText: {
    fontFamily: "minsk",
    fontSize: 30,
    marginTop: 40,
    marginBottom: 10,
    paddingTop: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#006073'
  },
  inputNameCaption: {
    fontFamily: "akrobat",
    fontSize: 20,
    marginTop: 10,
    marginHorizontal: 20,
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#006073'
  },
  inputName: {
    height: 100,
    marginHorizontal: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    borderColor: '#fbd78f',
    borderWidth: 1,
    backgroundColor: '#ffffff',
  },
  nextContainer: {
    marginTop: 20,
    alignItems: 'center',
  },
  nextLink: {
    backgroundColor: '#fbd78f',
    height: 40,
    width: 290,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  nextLinkText: {
    fontFamily: "akrobat",
    fontSize: 19,
    color: '#006073',
  },
});
